import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "../views/Home";
import Makanan from "../views/Makanan";
import Minuman from "../views/Minuman";
import NotFound from "../views/NotFound";

const Router = () => {
    return (
        <React.Fragment>
            <Switch>
                <Route path="/home" exact component={Home} />
                <Route path="/makanan" exact component={Makanan} />
                <Route path="/minuman" exact component={Minuman} />
                <Route component={NotFound} />
            </Switch>
        </React.Fragment>
    )
}

export default Router;