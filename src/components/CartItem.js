import React, { useState } from "react";
import styled from "styled-components";
import Counter from "./Counter";
import { useDispatch } from "react-redux";
import { dec, inc, remove } from "../store/action/Product";

const Cart = styled.div`
    display: flex;
    width: 90%;
    justify-content: space-between;
    align-items: center;
    height: 3rem;
    padding: 0 0.3rem;
    margin: 0.5rem auto;
    background: #fff;
    box-shadow: 1px 1px 10px 1px #ccc;
`
const CounterContainer = styled.div`
    display: flex;
    justify-content: space-between;
    width: 30%;
    text-align: center;
`
const Price = styled.div`
    wudth: 30%;
    text-align: center;
`
const ItemName = styled.div`
    width: 40%;
    text-align: left;
    padding-left: 0.5rem;
`
const CounterTotal = styled.div`
    margin: 0 0.3rem;

`

const CartItem = ({ item }) => {
    const dispatch = useDispatch()
    const [count, setCount] = useState(1)
    const increment = id => {
        setCount(count + 1)
        dispatch(inc(id))
    }
    const decrement = id => {
        if (count === 1) {
            dispatch(remove(id))
        } else if (count > 1 ){
            setCount(count - 1);
            dispatch(dec(id))
        }

    }
    return (
        <Cart>
            <ItemName>{item.name}</ItemName>
            <CounterContainer>
            <Counter inc={() => increment(item.id)}></Counter>
            <CounterTotal>{count}</CounterTotal>
            <Counter dec={() => decrement(item.id)}></Counter>
            </CounterContainer>
            <Price>{item.price}</Price>
        </Cart>
    )
}

export default CartItem