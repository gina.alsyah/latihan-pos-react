import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { reset } from "../store/action/Product";
import Button from "./Button";

const Box = styled.div`
    position: fixed;
    bottom: 0;
    width: 20rem;
    height: 8rem;
    box-shadow: 1px 1px 1px 1px #ccc;
    padding: 0.4rem;
    margin-left: -0.4rem;
`
const Total = styled.div`
    display: flex;
    justify-content: space-between;
    margin-bottom: 0.4rem;
`
const Pay = styled.div`
    display: flex;
    justify-content: space-between;
    margin-bottom: 0.4rem;
    input[type=number] {
        border: none;
        border-bottom: 1px solid black;
        font-weight: bold;
        text-align: right;
        &:focus {
            outline: none;
        }
        &::-webkit-inner-spin-button,
        &::-webkit-outer-spin-button{
            -webkit-appearance:none;
        }
    }
`
const Change = styled.div`
    display: flex;
    justify-content: space-between;
    margin-bottom: 0.6rem;
`
const BtnBox = styled.div`
    display: flex;
    justify-content: space-between;
    margin-bottom: 0.6rem;
`

const CalculateBox = () => {
    const dispatch = useDispatch()
    const carts = useSelector(state => state.product.carts)
    const totalVal = carts.reduce((total, current) => total = total + current.price, 0)
    const [pay, setPay] = useState("")
    const [change, setChange] = useState("")
    const handleChange = e => {
        setPay(e.target.value)
    }
    const calculateChange = () => {
        if (pay > totalVal) {
            setChange(pay - totalVal)
        }
    }
    const resetCart = () => {
        dispatch(reset())
        setPay("")
        setChange("")
    }
    return (
        <Box>
            <Total>
                <h4>Total</h4>
                <p>{totalVal}</p>
            </Total>
            <Pay>
                <h4>Jumlah Bayar</h4>
                <input type="number" value={pay} onChange={handleChange}/>
            </Pay>
            <Change>
                <h4>Kembalian</h4>
                <p>{change}</p>
            </Change>
            <BtnBox>
                <Button text="reset" action={resetCart}/>
                <Button primary text="bayar" action={calculateChange}/>
            </BtnBox>
        </Box>
    )
}

export default CalculateBox