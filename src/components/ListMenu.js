import React, { useState } from "react";
import styled from "styled-components";
import { Link, useLocation } from "react-router-dom";
import "../assets/ListMenu.css"

const Menu = styled.li`
    height: 2rem;
    display: flex;
    align-item: center;
    padding-left: 0.3rem;
    position: relative;
    color: ${props => props.theme.grey};
    cursor: pointer;
    &:not(:last-child) {
        margin:-bottom: 0.5rem;
    }
    textDecoration: "none",
    color: "#fff"
`

const ListMenu = () => {
    const [menu] = useState([
        "home", "makanan", "minuman"
    ])
    const pathname = useLocation().pathname
    console.log("ini", pathname)
    return(
        <ul>
            {menu.map((item, index) => 
                <Menu key={index} className={`${pathname === '/'+item ? 'link active' : 'link'}`}>
                    <Link to={`/${item}`} style={link}>{item}</Link>
                </Menu>    
            )}
        </ul>
    )
}

export default ListMenu

const link = {
    textDecoration: "none",
    color: "#ccc"
}