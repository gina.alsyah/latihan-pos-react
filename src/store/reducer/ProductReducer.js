import { products } from "../../utils/data"
const initialState = {
    products: products,
    carts: []
}
const productReducer = (state = initialState, action) => {
    const {type, payload} = action
    switch(type) {
        case "ADD_TO_CART":
            const itemInCart = state.carts.find(item => item.id === payload)
            const newItemCart = state.products.find(item => item.id === payload)
            if (!itemInCart) {
                return {
                    ...state,
                    carts: [...state.carts,newItemCart]
                }
            }
            return state
        case "INCREMENT":
            const oriPrice = state.products.find(item => item.id === payload).price
            const incCarts = state.carts.map(item => {
                if(item.id === payload){
                    return{
                        ...item,
                        price: item.price + oriPrice
                    }
                } else {
                    return item
                }
            })
            return {
                ...state,
                carts: incCarts
            }
        case "DECREMENT":
            const oriPrice2 = state.products.find(item => item.id === payload).price
            const incCarts2 = state.carts.map(item => {
                if(item.id === payload){
                    return{
                        ...item,
                        price: item.price - oriPrice2
                    }
                } else {
                    return item
                }
            })
            return {
                ...state,
                carts: incCarts2
            }
        case "REMOVE":
            return {
                ...state,
                carts: state.carts.filter(item => item.id !== payload)
            }
        case "RESET":
            return {
                ...state,
                carts: []
            }
        default:
            return state
    }
}

export default productReducer