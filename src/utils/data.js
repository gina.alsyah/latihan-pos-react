export const products = [
    {
      id: 1,
      name: "Salmon",
      image: require("../assets/foods/makanan1.jpg"),
      category: "makanan",
      price: 13000
    },
    {
      id: 2,
      name: "Maguro",
      image: require("../assets/foods/makanan2.jpg"),
      category: "makanan",
      price: 14000
    },
    {
      id: 3,
      name: "Ebi",
      image: require("../assets/foods/makanan3.jpg"),
      category: "makanan",
      price: 12000
    },
    {
      id: 4,
      name: "Tai",
      image: require("../assets/foods/makanan4.jpg"),
      category: "makanan",
      price: 15000
    },
    {
      id: 5,
      name: "Tako",
      image: require("../assets/foods/makanan5.jpg"),
      category: "makanan",
      price: 12000
    },
    {
      id: 6,
      name: "Hokkigai",
      image: require("../assets/foods/makanan6.jpg"),
      category: "makanan",
      price: 17000
    },
    {
      id: 7,
      name: "Unagi",
      image: require("../assets/foods/makanan7.jpg"),
      category: "makanan",
      price: 12000
    },
    {
      id: 8,
      name: "Saba",
      image: require("../assets/foods/makanan8.jpg"),
      category: "makanan",
      price: 19000
    },
    {
      id: 9,
      name: "Tamago",
      image: require("../assets/foods/makanan9.jpg"),
      category: "makanan",
      price: 12000
    },
    {
      id: 10,
      name: "Hotate",
      image: require("../assets/foods/makanan10.jpg"),
      category: "makanan",
      price: 12000
    },
    {
      id: 11,
      name: "Es teh",
      image: require("../assets/bevs/minuman1.jpg"),
      category: "minuman",
      price: 19000
    },
    {
      id: 12,
      name: "Es Jeruk",
      image: require("../assets/bevs/minuman2.jpg"),
      category: "minuman",
      price: 12000
    },
    {
      id: 13,
      name: "Es Cendol",
      image: require("../assets/bevs/minuman3.jpg"),
      category: "minuman",
      price: 12000
    }
  ]

  