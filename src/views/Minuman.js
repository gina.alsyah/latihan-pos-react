import React from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";
import ProductCard from "../components/ProductCard";
import MainLayout from "../layouts/MainLayout";

const MinumanContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;
const Minuman = () => {
  const minuman = useSelector((state) => state.product.products);
  const products = minuman.filter(item => item.category === "minuman")
  return (
    <MainLayout>
      <MinumanContainer>
        {products.map((product) => (
          <ProductCard key={product.id} item={product}></ProductCard>
        ))}
      </MinumanContainer>
    </MainLayout>
  );
};

export default Minuman;
