import React from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";
import ProductCard from "../components/ProductCard";
import MainLayout from "../layouts/MainLayout";

const ProductContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;
const Makanan = () => {
  const products = useSelector((state) => state.product.products).filter(item => item.category === "makanan");
  return (
    <MainLayout>
      <ProductContainer>
        {products.map((product) => (
          <ProductCard key={product.id} item={product}></ProductCard>
        ))}
      </ProductContainer>
    </MainLayout>
  );
};

export default Makanan;
